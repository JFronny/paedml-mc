package io.gitlab.jfronny.paedmlsupport;

import com.sun.jna.platform.win32.Kernel32;
import net.fabricmc.api.*;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;

import java.io.IOException;
import java.nio.file.*;

@Environment(EnvType.CLIENT)
public class PaedmlSupportClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        if (PaedmlMcConfig.loadMesa) {
            if (System.getProperty("os.name").startsWith("Windows")) {
                injectMesa();
            } else {
                PaedmlSupport.LOGGER.info("Detected non-Windows platform, skipping Mesa injection");
            }
        }
    }

    private static void injectMesa() {
        PaedmlSupport.LOGGER.info("Injecting Mesa OpenGL implementation");
        try {
            setEnv("MESA_GL_VERSION_OVERRIDE", "4.6FC");
            setEnv("MESA_GLSL_VERSION_OVERRIDE", "460");
            setEnv("GALLIUM_DRIVER", "llvmpipe");

            ModContainer mod = FabricLoader.getInstance().getModContainer(PaedmlSupport.MOD_ID).get();

            Path tempDir = Files.createTempDirectory("libmesa");

            Path dxil = tempDir.resolve("dxil.dll");
            Path libglapi = tempDir.resolve("libglapi.dll");
            Path opengl32 = tempDir.resolve("opengl32.dll");

            Files.copy(mod.findPath("dxil.dll").get(), dxil, StandardCopyOption.REPLACE_EXISTING);
            Files.copy(mod.findPath("libglapi.dll").get(), libglapi, StandardCopyOption.REPLACE_EXISTING);
            Files.copy(mod.findPath("opengl32.dll").get(), opengl32, StandardCopyOption.REPLACE_EXISTING);

            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    Files.delete(dxil);
                    Files.delete(libglapi);
                    Files.delete(opengl32);
                } catch (IOException e) {
                    PaedmlSupport.LOGGER.error("Could not remove leftover libraries, please do so manually", e);
                }
            }));

            System.load(dxil.toAbsolutePath().toString());
            System.load(libglapi.toAbsolutePath().toString());
            System.load(opengl32.toAbsolutePath().toString());
        } catch (Throwable e) {
            PaedmlSupport.LOGGER.error("Failed to inject Mesa", e);
        }
    }

    private static void setEnv(String name, String value) {
        if (!Kernel32.INSTANCE.SetEnvironmentVariable(name, value)) {
            throw new IllegalStateException("Could not set environment variable: " + name);
        }
    }
}
