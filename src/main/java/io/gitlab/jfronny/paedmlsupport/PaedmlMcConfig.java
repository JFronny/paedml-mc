package io.gitlab.jfronny.paedmlsupport;

import io.gitlab.jfronny.libjf.config.api.*;

public class PaedmlMcConfig implements JfConfig {
    @Entry public static Boolean loadMesa = true;

    @Category
    public static class Proxy {
        @Entry public static Boolean enabled = true;
        @Entry public static String address = "server.paedml-linux.lokal";
        @Entry public static Integer port = 3128;
        @Entry public static String username;
        @Entry public static String password;
    }
}
