package io.gitlab.jfronny.paedmlsupport;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Authenticator;

public class PaedmlSupport implements PreLaunchEntrypoint {
    public static final String MOD_ID = "paedml-mc";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    @Override
    public void onPreLaunch() {
        boolean authenticate = false;
        if (PaedmlMcConfig.Proxy.enabled) {
            if (System.getProperty("http.proxyHost") == null) {
                LOGGER.info("Configuring HTTP proxy");
                System.setProperty("http.proxyHost", PaedmlMcConfig.Proxy.address);
                if (PaedmlMcConfig.Proxy.port != null) System.setProperty("http.proxyPort", PaedmlMcConfig.Proxy.port.toString());
                if (PaedmlMcConfig.Proxy.username != null) {
                    authenticate = true;
                    System.setProperty("http.proxyUser", PaedmlMcConfig.Proxy.username);
                    if (PaedmlMcConfig.Proxy.password != null) System.setProperty("http.proxyPassword", PaedmlMcConfig.Proxy.password);
                }
            }
            if (System.getProperty("https.proxyHost") == null) {
                LOGGER.info("Configuring HTTPS proxy");
                System.setProperty("https.proxyHost", PaedmlMcConfig.Proxy.address);
                if (PaedmlMcConfig.Proxy.port != null) System.setProperty("https.proxyPort", PaedmlMcConfig.Proxy.port.toString());
                if (PaedmlMcConfig.Proxy.username != null) {
                    authenticate = true;
                    System.setProperty("https.proxyUser", PaedmlMcConfig.Proxy.username);
                    if (PaedmlMcConfig.Proxy.password != null) System.setProperty("https.proxyPassword", PaedmlMcConfig.Proxy.password);
                }
            }
            if (authenticate) {
                LOGGER.info("Configuring proxy authenticator");
                Authenticator.setDefault(new ProxyAuthenticator());
            }
        }
    }
}
