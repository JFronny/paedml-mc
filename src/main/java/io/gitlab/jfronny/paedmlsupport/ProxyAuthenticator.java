package io.gitlab.jfronny.paedmlsupport;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class ProxyAuthenticator extends Authenticator {
    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        if (getRequestorType() == RequestorType.PROXY) {
            String protocol = getRequestingURL().getProtocol();
            String proxyUser = System.getProperty(protocol + ".proxyUser");

            if (proxyUser != null) {
                String proxyPassword = System.getProperty(protocol + ".proxyPassword");

                if (proxyPassword == null) {
                    proxyPassword = "";
                }

                return new PasswordAuthentication(proxyUser, proxyPassword.toCharArray());
            }
        }

        return super.getPasswordAuthentication();
    }
}
